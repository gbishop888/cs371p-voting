#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <iterator>
using namespace std;


//collects all the names of the candidates
vector<string> getCandidates(istream& r) {
    string temp;
    getline(r, temp);
    istringstream parse(temp);
    int num;
    
    parse >> num;
    vector<string> candidateList(num);
    for (int i = 0; i < num; i++) {
        temp = "";
        getline(r, temp);
        candidateList[i] = temp;
    }
    return candidateList;
}

//collects all the ballots
vector<vector<int>> getBallots(istream& r, int num) {
    string temp;
    getline(r, temp);
    vector<vector<int>> ballotList;
    bool end = true;
    while (!temp.empty() && end) {
        if (!r.good()) {
            end = false;
        }
        istringstream parseString(temp);
        vector<int> votes;
        for (int i = 0; i < num; i++) {
            int x;
            parseString >> x;
            votes.push_back(x);
        }
        ballotList.push_back(votes);
        getline(r, temp);
    }
    return ballotList;
}

//splits the ballots based of who they are voting for
vector<vector<vector<int>>> splitBallots(int numCandidates, vector<vector<int>>& ballots) {
    vector<vector<vector<int>>> sortedBallots;
    for (int i = 0; i < numCandidates; i++) {
        vector<vector<int>> sort;
        for (int j = 0; j < ballots.size(); j++) {
            if (i + 1 == ballots[j][0]) {
                sort.push_back(ballots[j]);
            }
        }
        sortedBallots.push_back(sort);
    }
    return sortedBallots;
}

//checks if there is a majority for the ballots
int checkMajority(vector<vector<vector<int>>>& votes, int numCandidates, vector<vector<int>>& ballots) {
    for (int j = 0; j < numCandidates; j++) {
        if (votes[j].size() > (ballots.size()/2)) {
            return j;
        }
    }
    return -1;
}

//checks if there are any people tying
vector<int> checkTie(vector<vector<vector<int>>>& votes, int numCandidates, vector<vector<int>>& ballots) {
    vector<int> winners;    
    for (int j = 0; j < numCandidates; j++) {
        if (votes[j][0][0] != -1) {
            winners.push_back(j);
            for (int i = j+1; i < numCandidates; i++) {
                if (votes[i][0][0] != -1 && votes[j].size() != votes[i].size()) {
                    winners[0] = -1;
                    return winners;
                }
                winners.push_back(i);
            }
            break;
        }
    }
    return winners;
}

//finds all the people with the lowest amount of votes
vector<int> findLoser(vector<vector<vector<int>>>& votes, int numCandidates) {
    int lowest = votes[0].size();
    vector<int> index;
    for (int i = 1; i < numCandidates; i++) {
        if (lowest > votes[i].size() && votes[i][0][0] != -1) {
            lowest = votes[i].size();
        } 
    }
    //find duplicates
    for (int i = 0; i < numCandidates; i++) {
        if (lowest == votes[i].size() && votes[i][0][0] != -1) {
            index.push_back(i);
        } 
    }
    return index;
}

//reallocates the votes to the people still in the race
vector<vector<vector<int>>> reallocate(vector<int> loser, vector<vector<vector<int>>>& votes, int numCandidates) {
    //cycle through each ballot
    bool alsoLoser = false;
    //recurs for each tied loser
    for (int a = 0; a < loser.size(); a++) {
        //each ballot of the loser
        for (int i = 0; i < votes[loser[a]].size(); i++) {
            int n = 1;
            for (int z = 1; z < numCandidates; z++) {
                if ( votes[loser[a]][i][z] != -1) {
                    break;
                }
                n++;
            }
            //compares with each candidate
            for (int j = 0; j < numCandidates; j++) {
                //finds other losers
                if (j != loser[a] && j+1 == votes[loser[a]][i][n]) {
                    for (int y = 0; y < loser.size(); y++) {
                        if (j == loser[y]) {
                            votes[loser[a]][i][n] = -1;
                            n++;
                            alsoLoser = true;
                        }
                    }
                    if (!alsoLoser) {
                        vector<vector<int>> temp;
                        temp.push_back(votes[loser[a]][i]);
                        temp[0][n-1] = -1;
                        votes[j].push_back(temp[0]);
                        j == numCandidates;
                    } else {
                        j = 0;
                        alsoLoser = false;
                    }   
                }
            }
        }
    votes[loser[a]][0][0] = -1;
    }
    return votes;
}

//runs everything else
vector<string> voting(istream& r, ostream& w) {
    vector<string> winnerName;
    vector<string> candidates = getCandidates(r);
    if (candidates.size() == 1) {
        winnerName.push_back(candidates[0]);
        return winnerName;
    }
    int numCandidates = candidates.size();
    vector<vector<int>> ballots = getBallots(r, numCandidates);
    vector<vector<vector<int>>> votes = splitBallots(numCandidates, ballots);
    int winner  = checkMajority(votes, numCandidates, ballots);
    while (winner == -1) {
        vector<int> tie = checkTie(votes, numCandidates, ballots);
        if (tie[0] != -1) {
            for (int i = 0; i < tie.size(); i++) {
                winnerName.push_back(candidates[i]);
            }
            return winnerName;
        }
        vector<int> loserIndex = findLoser(votes, numCandidates);
        vector<vector<vector<int>>> newVotes;
        for (int i = 0; i < loserIndex.size(); i++) {
            newVotes = reallocate(loserIndex, votes, numCandidates);
        }
        winner = checkMajority(newVotes, numCandidates, ballots);
    }
    winnerName.push_back(candidates[winner]);
    return winnerName;
}

//returns the winners
void allWinners(istream&r, ostream& w) {
    string temp;
    getline(r, temp);
    istringstream parse(temp);
    int numElections;
    parse >> numElections;
    if (numElections == 0) {
        w << " " << endl;
    } else {
        getline(r, temp);
        for (int i = 0; i < numElections; i++) {
            vector<string> winner = voting(r, w);
            for (int j = 0; j < winner.size(); j++) {
                w << winner[j] << endl;
            }
            cout << "" << endl;
        }
    }
}